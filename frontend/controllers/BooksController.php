<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/30/2016
 * Time: 11:40 PM
 */

namespace frontend\controllers;


use avega\F;
use common\models\Book;
use common\models\Image;
use common\models\Section;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BooksController extends Controller
{
    /**
     * Страница с книгами.
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'books' => Book::find()
                ->orderBy('id DESC')
                ->asArray()
                ->all()
        ]);
    }

    /**
     * Страница всех глав книги.
     */
    public function actionView($id)
    {
        $book = Book::findOne($id);

        if ($book !== null) {
            return $this->render('view', [
                'sections' => Section::findAll(['bookId' => $id]),
                'book' => $book
            ]);
        } else {
            throw new NotFoundHttpException;
        }
    }

    /**
     * Страница просмотра изображений главы.
     */
    public function actionSection($id, $imageId = null)
    {
        $section = Section::findOne($id);

        if ($section !== null) {
            $book = Section::findBookBySectionId($id);

            return $this->render('images',
                [
                    'nextSection' => $section->getNextSection(),
                    'prevSection' => $section->getPrevSection(),
                    'book' => $book,
                    'section' => $section,
                    'images' => Image::find()
                        ->where(['sectionId' => $id])
                        ->select('id')
                        ->asArray()
                        ->all(),
                    'imageId' => $imageId
                ]);
        } else {
            throw new NotFoundHttpException;
        }
    }
}