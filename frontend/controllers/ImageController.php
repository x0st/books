<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 8/1/2016
 * Time: 12:59 PM
 */

namespace frontend\controllers;


use frontend\components\filters\AjaxAccess;
use common\models\Image;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ImageController extends Controller
{
    public function behaviors()
    {
        return [
            'verb' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-image' => ['post'],
                    'abracadabra' => ['put']
                ]
            ],
            'ajax' => [
                'class' => AjaxAccess::className(),
                'only' => ['get-image', 'abracadabra']
            ]
        ];
    }

    /**
     * Отдает изображение по id.
     */
    public function actionGetImage($id)
    {
        $image = Image::findOne($id);

        $abracadabra = \Yii::$app->request->post('abracadabra');

        if ($image !== null && $this->checkAbracadabra($abracadabra)) {
            $imagePath = \Yii::getAlias(\Yii::$app->params['paperPath'] . $image->file);
            $type = pathinfo($imagePath, PATHINFO_EXTENSION);
            $image = file_get_contents($imagePath);
//            $response = \Yii::$app->getResponse();
//            $response->headers->set('Content-Type', 'image/jpeg');
//            $response->format = Response::FORMAT_RAW;
//
//            if (!is_resource($response->stream = fopen($imagePath, 'r'))) {
//                throw new ForbiddenHttpException;
//            }
//
//            return $response->send();
            return 'data:image/' . $type . ';base64,' . base64_encode($image);
        }

        throw new NotFoundHttpException;
    }

    /**
     * Возвращает ключ для доступа к изображениям.
     */
    public function actionAbracadabra()
    {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        return $this->generateAbracadabra();
    }

    /**
     * Верификация ключа.
     */
    private function checkAbracadabra($key)
    {
        return \Yii::$app->session->getFlash('abracadabra') === $key;
    }

    /**
     * Генерация ключа.
     */
    private function generateAbracadabra()
    {
        $key = \Yii::$app->security->generateRandomString();
        \Yii::$app->session->setFlash('abracadabra', $key);

        return $key;
    }
}