<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

\frontend\assets\BootstrapAsset::register($this);
\frontend\assets\AdminLTEAsset::register($this);
\frontend\assets\FontAwesomeAsset::register($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <div class="header">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="logo">
                                <img src="<?= \yii\helpers\Url::to(['/images/logo.png']) ?>" alt="Логотип">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="right-top-menu">
                                <ul>
                                    <li>
                                        <a href="">Главная</a>
                                    </li>
                                    <li>
                                        <a href="">О проекте</a>
                                    </li>
                                    <li>
                                        <a href="">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $content ?>
<!--    --><?php //if (Yii::$app->controller->action->id !== 'section'): ?>
        <div class="navbar-fixed-top footer row-fluid content">
            <div class="">
                <div class="container">
                    Все права защищены © <?= date('Y') ?> <a
                        href="<?= \yii\helpers\Url::to(Yii::$app->homeUrl) ?>"><?= Yii::$app->name ?></a>. Разработано
                    компанией <?= Yii::$app->params['poweredBy'] ?>.
                </div>
            </div>
        </div>
<!--    --><?php //endif ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
