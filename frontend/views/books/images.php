<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 8/1/2016
 * Time: 1:00 PM
 * @var $images array. массив id изображений
 * @var $book \common\models\Book
 * @var $section \common\models\Section
 * @var $imageId integer
 * @var $nextSection \common\models\Section
 * @var $prevSection \common\models\Section
 */
?>
<div class="content">
    <div class="image-view">
        <div class="row section-navigation">
            <div class="col-xs-4">
                <a class="prev-section"
                   href="<?= $prevSection === null ? '#' : \yii\helpers\Url::to(['/books/section/' . $prevSection->id]) ?>" <?= $prevSection === null ? 'disabled' : '' ?>>
                    Глава
                </a>
            </div>
            <div class="col-xs-4">
                <a href="<?= \yii\helpers\Url::to(['/books/view/' . $book['id']]) ?>" class="to-all-sections">Оглавление</a>
            </div>
            <div class="col-xs-4">
                <a class="next-section"
                   href="<?= $nextSection === null ? '#' : \yii\helpers\Url::to(['/books/section/' . $nextSection->id]) ?>" <?= $nextSection === null ? 'disabled' : '' ?>>
                    Глава
                </a>
            </div>
        </div>
        <div class="row">
            <?php if (count($images) > 0): ?>
                <div id="image">
                    <a href="#" class="left">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    <a href="#" class="right">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                </div>
            <?php else: ?>
                <h3>В данной главе еще нет страниц.</h3>
            <?php endif ?>
        </div>
    </div>
</div>
<script>
    <?php if (count($images) > 0): ?>
    /**
     * Массив id изображений.
     */
    var ids = <?= json_encode($images) ?>;

    var Abracadabra = {
        /**
         * Возвращает токен.
         * @return {string}
         */
        get: function () {
            return $.ajax({
                url: '<?= \yii\helpers\Url::to(['/image/abracadabra']) ?>',
                type: 'PUT',
                async: false,
                data: {_csrf: '<?= Yii::$app->request->getCsrfToken() ?>'}
            }).responseText;
        }
    };

    var Image = {
        selectors: {
            image: '#image',
            left: '.left',
            right: '.right'
        },

        /**
         * id в бд первого изображения чтобы понять, что влево нельзя уже листать.
         */
        firstImageId: <?= $images[0]['id'] !== null ? $images[0]['id'] : 0 ?>,
        /**
         * id в бд последнего изображения чтобы понять, что вправо нельзя уже листать.
         */
        lastImageId: <?= $images[count($images) - 1]['id'] !== null ? $images[count($images) - 1]['id'] : 0 ?>,
        /**
         * Указатель на индекс в массиве изображений. Изначально 0, т.к. при заходе на страницу показывается первое изображение главы.
         */
        imagePointer: <?= 0 ?>,

        /**
         * Запрос на загрузку изображения по id, если указан.
         * @returns {boolean}
         */
        loadImage: function (id) {

            if (id === false) {
                id = ids[this.imagePointer].id;
            }

            var _this = this;

            $.ajax({
                url: '<?= \yii\helpers\Url::to(['/image/get-image']) ?>/' + id,
                type: 'POST',
                data: {_csrf: '<?= Yii::$app->request->getCsrfToken() ?>', abracadabra: Abracadabra.get()},
                success: function (response) {
                    _this.afterLoad(response, id);
                },
                error: function () {
                    alert('Ошибка загрузки изображения');
                }
            });
        },
        /**
         * После ajax запроса в фк-цию передается base64 код изображения.
         * @param id
         * @param response
         */
        afterLoad: function (response, id) {
            window.history.pushState({id: id}, 'Изображение №' + id, '<?= \yii\helpers\Url::to(['/books/section/' . $section->id]) ?>' + '/' + id);
            $(this.selectors.image).css({'background': 'url(' + response + ' )'});
        },
        /**
         * Листаем вправо.
         */
        incPointer: function () {
            this.imagePointer++;
        },
        /**
         * Листаем влево.
         */
        decPointer: function () {
            this.imagePointer--;
        }
    };

    /**
     * Клик по кнопке "предыдущее изображени"
     */
    $(Image.selectors.left).click(function () {
        // проверка, существует ли предыдущее изображение
        if (typeof ids[Image.imagePointer - 1] !== typeof undefined) {
            Image.decPointer();
            Image.loadImage(false);
        }
        return false;
    });

    /**
     * Клик по кнопке "следующее изображение"
     */
    $(Image.selectors.right).click(function () {
        // проверка, существует ли следующее изображение
        if (typeof ids[Image.imagePointer + 1] !== typeof undefined) {
            Image.incPointer();
            Image.loadImage(false);
        }
        return false;
    });

    $(function () {
        console.log(window.location.href);
        // если юзверь перешел на конкретное изображение
        // тогда проверяем, есть ли оно в массиве изображений
        // если да, вставляем id изображения, иначе false
        Image.loadImage(<?= $imageId !== null ? (array_search($imageId, array_column($images, 'id')) ? $imageId : 'false') : 'false' ?>);
    });
    <?php endif ?>
</script>
