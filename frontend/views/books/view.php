<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 8/1/2016
 * Time: 10:47 AM
 * @var $sections
 * @var $book
 */
?>
<div class="content">
    <div class="container">
        <div class="section-book-name">
            <a href="<?= \yii\helpers\Url::to(['/books']) ?>">Книги</a> &raquo; <?= $book->title ?>
        </div>
        <?php if (count($sections) > 0): ?>
            <?php foreach ($sections as $section): ?>
                <div class="section-container">
                    <a href="<?= \yii\helpers\Url::to(['/books/section/' . $section['id']]) ?>"><?= $section->name ?></a>
                </div>
            <?php endforeach ?>
        <?php else: ?>
            <h4>В данной книге еще нет глав.</h4>
        <?php endif ?>
    </div>
</div>
