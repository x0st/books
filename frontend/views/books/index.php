<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/30/2016
 * Time: 11:52 PM
 * @var $books array
 */
$thumbnailWidth = \backend\models\ImagesForm::THUMBNAIL_WIDTH;
$thumbnailHeight = \backend\models\ImagesForm::THUMBNAIL_HEIGHT;
$thumbnailPrefix = $thumbnailWidth . 'x' . $thumbnailHeight . '_';
\backend\assets\LightBox2Asset::register($this);
?>
<div class="content">
    <div class="container">
        <div class="book-list">
            <?php foreach ($books as $book): ?>
                <div class="book-container">
                    <div class="cover-container">
                        <a data-lightbox="thumbnails" href="<?= Yii::$app->params['coverUrl'] . $book['cover'] ?>">
                            <img
                                src="<?= Yii::getAlias(Yii::$app->params['coverUrl'] . $thumbnailPrefix . $book['cover']) ?>"
                                alt="Обложка">
                        </a>
                    </div>
                    <div class="info-container">
                        <a href="<?= \yii\helpers\Url::to(['/books/view/' . $book['id']]) ?>"><?= $book['title'] ?></a>
                        <p><?= $book['description'] ?></p>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>