<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 5/31/2016
 * Time: 11:03 PM
 */

namespace frontend\components\filters;

use yii;
use yii\base\ActionFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class AjaxAccess extends ActionFilter
{
	public $defaultUrl = ['/'];
	
	/**
	 * Declares event handlers for the [[owner]]'s events.
	 * @return array events (array keys) and the corresponding event handler methods (array values).
	 */
	public function events()
	{
		return [Controller::EVENT_BEFORE_ACTION => 'beforeAction'];
	}
	
	/**
	 * @param yii\base\Action $event
	 * @return boolean
	 * @throws yii\web\MethodNotAllowedHttpException when the request method is not allowed.
	 */
	public function beforeAction($event)
	{
		if (Yii::$app->request->isAjax)
			return parent::beforeAction($event);
		else
			$this->denyAccess(Yii::$app->user);
	}
	
	/**
	 * Denies the access of the user.
	 * The default implementation will redirect the user to the login page if he is a guest;
	 * if the user is already logged, a 403 HTTP exception will be thrown.
	 * @param yii\web\User $user the current user
	 * @throws yii\web\ForbiddenHttpException if the user is already logged in.
	 */
	protected function denyAccess($user)
	{
//		if ($user->getIsGuest()) {
//			$user->loginRequired();
//		} else {
			$this->ajaxOnly();
//		}
	}
	
	public function ajaxOnly()
	{
		if ($this->defaultUrl !== null) {
			$defUrl = (array)$this->defaultUrl;
			if ($defUrl[0] !== Yii::$app->requestedRoute)
				return Yii::$app->getResponse()->redirect($this->defaultUrl);
		}
		throw new ForbiddenHttpException('Only ajax!');
	}
}