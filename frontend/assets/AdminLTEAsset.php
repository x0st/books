<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/24/2016
 * Time: 3:26 PM
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class AdminLTEAsset extends AssetBundle
{
	public $jsOptions = [
		'position' => \yii\web\View::POS_HEAD
	];
	
	public $css = [
		'bower/AdminLTE/dist/css/AdminLTE.min.css',
		'bower/AdminLTE/dist/css/skins/_all-skins.css'
	];

	public $depends = [
		'backend\assets\JqueryAsset'
	];
}