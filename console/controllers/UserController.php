<?php

namespace console\controllers;
use common\models\User;

/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 05.08.16
 * Time: 17:59
 */
class UserController extends \yii\console\Controller
{
    public function actionCreateAdmin()
    {
        $user = new User();
        $user->email = 'admin@email.com';
        $user->username = 'admin';
        $user->setPassword('111111');
        $user->save();
    }
}