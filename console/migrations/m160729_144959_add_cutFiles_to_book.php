<?php

use yii\db\Migration;

class m160729_144959_add_cutFiles_to_book extends Migration
{
    public function up()
    {
        $this->addColumn('book', 'cutFiles', $this->boolean()->comment('при заливке изображений резать каждый файл пополам или нет'));
    }

    public function down()
    {
        $this->dropColumn('book', 'cutFiles');
    }
}
