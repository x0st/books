<?php

use yii\db\Migration;

/**
 * Handles the creation for table `section`.
 */
class m160727_185400_create_section_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('section', [
            'id' => $this->primaryKey(),
            'name' => $this->string(500)
        ], 'charset=utf8');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('section');
    }
}
