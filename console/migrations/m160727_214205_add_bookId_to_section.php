<?php

use yii\db\Migration;

class m160727_214205_add_bookId_to_section extends Migration
{
    public function up()
    {
        $this->addColumn('section', 'bookId', $this->bigInteger(21));
    }

    public function down()
    {
        $this->dropColumn('section', 'bookId');
    }
}
