<?php

use yii\db\Migration;

class m160729_165347_rename_table_paper extends Migration
{
    public function up()
    {
        $this->renameTable('paper', 'image');
    }

    public function down()
    {
        $this->renameTable('image', 'paper');
    }
}
