<?php

use yii\db\Migration;

/**
 * Handles the creation for table `paper`.
 */
class m160727_214008_create_paper_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('paper', [
            'id' => $this->primaryKey(),
            'file' => $this->string(500),
            'sectionId' => $this->bigInteger(21)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('paper');
    }
}
