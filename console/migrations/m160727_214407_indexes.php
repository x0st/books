<?php

use yii\db\Migration;

class m160727_214407_indexes extends Migration
{
    public function up()
    {
        $this->createIndex('FastSearch', 'section', 'bookId');
        $this->createIndex('FastSearch', 'paper', 'sectionId');
    }

    public function down()
    {
        $this->dropIndex('FastSearch', 'section');
        $this->dropIndex('FastSearch', 'paper');
    }
}
