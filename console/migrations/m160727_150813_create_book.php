<?php

use yii\db\Migration;

class m160727_150813_create_book extends Migration
{
    public function up()
    {
        $this->createTable('book', [
            'id' => $this->primaryKey(),
            'title' => $this->string(500),
            'cover' => $this->string(100),
            'description' => $this->string(500)
        ], 'charset=utf8');
    }

    public function down()
    {
        $this->dropTable('book');
    }

}
