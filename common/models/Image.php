<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/28/2016
 * Time: 1:11 PM
 */

namespace common\models;


use avega\F;
use backend\models\ImagesForm;
use yii\db\ActiveRecord;

/**
 * Class Image
 * @package common\models
 * @property $id integer
 * @property $file string
 * @property $sectionId integer
 */
class Image extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * Удаляет изображение главы по id или по name.
     * @param null|int $id
     * @param null|string $file
     * @param int $sectionId
     * @return bool
     */
    public static function remove($id = null, $file = null, $sectionId)
    {
        $image = self::find()
            ->where(['id' => $id])
            ->orWhere(['file' => $file])
            ->andWhere(['sectionId' => $sectionId])
            ->one();

        $thumbnailWidth = ImagesForm::THUMBNAIL_WIDTH;
        $thumbnailHeight = ImagesForm::THUMBNAIL_HEIGHT;
        $thumbnailPrefix = $thumbnailWidth . 'x' . $thumbnailHeight . '_';

        if ($image !== null) {
            unlink(\Yii::getAlias(\Yii::$app->params['paperPath'] . $image->file));
            unlink(\Yii::getAlias(\Yii::$app->params['paperPath'] . $thumbnailPrefix . $image->file));
            return (bool)$image->delete();
        }

        return false;
    }

    /**
     * Обновляет порядок изображений в главе.
     * @param array $images
     * @param $sectionId
     * @return boolean
     */
    public static function updateImages($images, $sectionId)
    {
        // проверка на существование каждого файла
        foreach ($images as $image) {
            if (!file_exists(\Yii::getAlias(\Yii::$app->params['paperPath'] . $image))) {
                return false;
            }
        }

        $images = array_map(function ($image) use ($sectionId) {
            return ['file' => $image, 'sectionId' => $sectionId];
        }, $images);

        self::deleteAll(['sectionId' => $sectionId]);

        return \Yii::$app->db
            ->createCommand()
            ->batchInsert(self::tableName(), ['file', 'sectionId'], $images)
            ->execute() == count($images);
    }
}