<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/28/2016
 * Time: 12:54 PM
 */

namespace common\models;


use backend\models\ImagesForm;
use yii\db\ActiveRecord;

/**
 * Class Section
 * @package common\models
 * @property $id integer
 * @property $name string
 * @property $bookId integer
 */
class Section extends ActiveRecord
{
    /**
     * Страндартное имя главы.
     */
    const DEFAULT_NAME = 'Глава без имени';

    /**
     * Имя таблицы.
     * @return string
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * Обновляет имя главы.
     * @param $id
     * @param $newName
     * @return bool
     */
    public static function updateName($id, $newName)
    {
        if (empty($newName)) {
            return false;
        }

        return (bool)self::updateAll(['name' => $newName], ['id' => $id]);
    }

    /**
     * Удаляет изображения главы, главу.
     * @param $id
     */
    public static function remove($id)
    {
        // изображения главы
        $papers = Image::findAll(['sectionId' => $id]);

        $thumbnailWidth = ImagesForm::THUMBNAIL_WIDTH;
        $thumbnailHeight = ImagesForm::THUMBNAIL_HEIGHT;
        $thumbnailPrefix = $thumbnailWidth . 'x' . $thumbnailHeight . '_';

        // удаляем все изображения главы
        foreach ($papers as $paper) {
            unlink(\Yii::getAlias(\Yii::$app->params['paperPath'] . $paper['file']));
            unlink(\Yii::getAlias(\Yii::$app->params['paperPath'] . $thumbnailPrefix . $paper['file']));
        }

        Image::deleteAll(['sectionId' => $id]);
        self::deleteAll(['id' => $id]);
    }

    /**
     * Создает главу со стандартным именем для книги с id $id.
     * @param $id
     * @return bool|integer
     */
    public static function createDefaultSection($id)
    {
        $model = new self;

        $model->name = self::DEFAULT_NAME;
        $model->bookId = $id;

        return $model->save(false) ? $model->id : false;
    }

    /**
     * Находит книгу по id её главы.
     * @param $id
     * @return array|null|ActiveRecord
     */
    public static function findBookBySectionId($id)
    {
        $sectionTableName = self::tableName();
        $bookTableName = Book::tableName();

        return self::find()
            ->where(["{$sectionTableName}.id" => $id])
            ->select(["{$bookTableName}.id", "{$bookTableName}.cutFiles", "{$sectionTableName}.bookId"])
            ->innerJoin("{$bookTableName}", "{$bookTableName}.id = {$sectionTableName}.bookId")
            ->asArray()
            ->one();
    }

    /**
     * Функция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['sectionId' => 'id']);
    }

    /**
     * Возвращает информацию о следующей главе.
     * @return null|ActiveRecord
     */
    public function getNextSection()
    {
        return self::find()
            ->where(['>', 'id', $this->id])
            ->andWhere(['bookId' => $this->bookId])
            ->limit(1)
            ->one();
    }


    /**
     * Возвращает информацию о предыдущей главе.
     * @return null|ActiveRecord
     */
    public function getPrevSection()
    {
        return self::find()
            ->where(['<', 'id', $this->id])
            ->andWhere(['bookId' => $this->bookId])
            ->limit(1)
            ->one();
    }
}