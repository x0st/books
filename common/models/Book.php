<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/27/2016
 * Time: 8:14 PM
 */

namespace common\models;


use backend\models\BookForm;
use yii\db\ActiveRecord;

/**
 * Class Book
 * @package common\models
 * @property $id integer
 * @property $title string
 * @property $cover string
 * @property $description string
 * @property $cutFiles boolean
 */
class Book extends ActiveRecord
{
    public static function tableName()
    {
        return 'book';
    }

    /**
     * Функция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Section::className(), ['bookId' => 'id']);
    }

    /**
     * Удаляет книгу, главы в ней, изображения в каждой главе.
     * @param $id
     * @return bool
     */
    public static function remove($id)
    {
        $book = self::findOne($id);

        $sectionTableName = Section::tableName();
        $bookTableName = self::tableName();
        $imageTableName = Image::tableName();

        $images = self::find()
            ->select(["{$imageTableName}.file", "{$imageTableName}.id"])
            ->innerJoin($sectionTableName, "{$sectionTableName}.bookId = {$bookTableName}.id")
            ->innerJoin($imageTableName, "{$imageTableName}.sectionId = {$sectionTableName}.id")
            ->where(["{$bookTableName}.id" => $id])
            ->asArray()
            ->all();

        foreach ($images as $image) {
            unlink(\Yii::getAlias(\Yii::$app->params['paperPath'] . $image['file']));
        }

        unlink(\Yii::getAlias(\Yii::$app->params['coverPath'] . $book->cover));

        // получаем массив id изображений чтобы удалить их все разом из бд
        $imagesId = array_map(function ($image) {
            return $image['id'];
        }, $images);

        // удаляет все изображения книги
        Image::deleteAll(['id' => $imagesId]);
        Section::deleteAll(['bookId' => $id]);

        // если кол-во затронутых строк == 1, тогда удаление прошло успешно
        return (bool)$book->delete();
    }

    /**
     * Удаляет существующую обложку.
     */
    protected function deleteCurrentCover() {
        if (!empty($this->getOldAttribute('cover'))) {
            $thumbnailPrefix = BookForm::THUMBNAIL_WIDTH . 'x' . BookForm::THUMBNAIL_HEIGHT . '_';

            unlink(\Yii::getAlias(\Yii::$app->params['thumbnailPath'] . $this->getOldAttribute('cover')));
            unlink(\Yii::getAlias(\Yii::$app->params['thumbnailPath'] . $thumbnailPrefix . $this->getOldAttribute('cover')));
        }
    }
}