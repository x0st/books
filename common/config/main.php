<?php
return [
    'version' => '1.0',
    'name' => 'Полтавика',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ]
    ],
];
