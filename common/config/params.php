<?php
return [
    'poweredBy' => 'Avega Group',
    'coverPath' => '@frontend/web/covers/',
    'thumbnailPath' => '@frontend/web/covers/',
    'coverUrl' => 'http://books.com/covers/',
    'paperPath' => '@frontend/web/papers/'
];
