/**
 * Created by Administrator on 23/04/2016.
 */

var body = $('body');

function trim(str) {
    var len = str.length;
    if (str.charCodeAt(0) == 32) str = trim(str.substring(1, len));
    if (str.charCodeAt(len - 1) == 32) str = trim(str.substring(0, len - 1));
    return str;
}

function isEmpty(str) {
    return trim(str) == '';
}

function showAlert(type, messages) {
    var containerSelector;

    if (type == 1) {
        type = 'success';
    } else if (type == 2) {
        type = 'warning';
    } else if (type == 0) {
        type = 'error';
    }

    if (typeof type == typeof 'string') {
        containerSelector = '.' + type + '_container';
    } else if (typeof type == typeof {}) {
        containerSelector = type;
    }


    $('span', $(containerSelector)).html('');

    if (typeof messages == typeof 'text') {
        $('span', $(containerSelector)).append('<div>' + messages + '</div>');
    } else {
        $.each(messages, function (index, value) {
            if (typeof value == typeof {}) {
                $.each(value, function (i, mess) {
                    $('span', $(containerSelector)).append('<div>' + mess + '</div>');
                });
            } else {
                $('span', $(containerSelector)).append('<div>' + value + '</div>');
            }
        });
    }

    $(containerSelector).show();
}

function hideAlert(type) {
    if (typeof type === typeof undefined) {
        var containers = ['.success_container', '.error_container', '.warning_container'];

        for (var i = 0; i < containers.length; i++) {
            $(containers[i]).hide();
            $('span', $(containers[i])).html('');
        }
    }else{
        var selector_container;

        if (typeof type == 'string') {
            selector_container = '.' + type + '_container';
        } else if (typeof type == 'object') {
            selector_container = type;
        }

        $(selector_container).hide();
        $('span', $(selector_container)).html('');
    }
}