/**
 * Форматирует строку 'string-str' к виду 'StringStr' или 'string_dsa-d' к 'StringDsaD'.
 * @param string
 * @returns {string}
 */
function processString(string) {
    var newName = '';

    var nextToUp = true;

    for (var i = 0; i < string.length; i++) {
        if (string[i] === '-' || string[i] === '_') {
            nextToUp = true;
        } else {
            newName += nextToUp === true ? (string[i]).toUpperCase() : string[i];
            nextToUp = false;
        }
    }

    return newName;
}

var body = $('body');

var Form = (function () {
    function Form(name, config) {
        /**
         * Имя формы чтобы отличать форму, в случае если на странице их несколько.
         * @type {string}
         */
        this.name = name;
        /**
         * Название события.
         * @type {string}
         */
        this.action = 'create';
        /**
         * Урл, куда делать запрос через ajax.
         * @type {string}
         */
        this.url = 'http://yahoo.com';
        /**
         * Селектор кнопки, чтобы повесить на неё обработчик нажатия.
         * @type {string}
         */
        this.submitButton = '.btn-create';
        /**
         * Массив id полей, с которых будут браться данные.
         * ['name', 'password', 'email']
         * @type {Array}
         */
        this.fields = [];
        /**
         * Селекторы сообщений, в случае необходимости отображения сообщения
         * @type {{success: string, error: string, warning: string}}
         */
        this.containers = {
            success: '.success_container',
            error: '.error_container',
            warning: '.warning_container'
        };

        var _this = this;

        if (typeof config == typeof {}) {
            $.each(config, function (property, value) {
                _this[property] = value;
            });
        }

        // если не был указан селектор кнопки, чтобы повесить на неё обработчик,
        // тогда селектором будет шаблон '.btn-' + название события.
        // например '.btn-cancel-operation', '.btn-create'
        if (typeof config.submitButton === typeof undefined) {
            this.submitButton = '.btn-' + config.action;
        }

        // вешаем обработчик клика на кнопку
        $(_this.submitButton).click(function () {
            _this.submit();
        });
    }

    Form.prototype = {
        /**
         * Отправляет запрос на сервер, со всеми значениями полей.
         */
        submit: function () {
            var _this = this;

            this.hideAllMessages();

            var data = _this.compileFields();
            var beforeSubmit = _this.beforeSubmit(data.form);

            // если фк-ция beforeSubmit() была переопределенна, там производились какие-то действия,
            // но фк-ция вернула значение отличное от false
            if (beforeSubmit !== false) {
                // нужно добавить какие-то данные в массив, который отправиться на сервер
                if (typeof beforeSubmit === typeof {} || typeof beforeSubmit === typeof []) {
                    $.each(beforeSubmit, function (propertyName, propertyValue) {
                        data.form[propertyName] = propertyValue;
                    });
                }

                // хрень на весь экран, кружочек loading
                body.addClass('loading-custom');

                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: _this.url,
                    data: data,
                    success: function (response) {
                        if (typeof response.messages == 'string') {
                            response.messages = [response.messages];
                        }

                        // запускаем фк-цию например afterCreate, в которую передаем ответ сервера
                        // и данные из полей
                        _this['after' + processString(_this.action)].apply(_this, [response, data.form]);

                        body.removeClass('loading-custom');
                    },
                    error: function () {
                        alert('Unknown error');
                        body.removeClass('loading-custom');
                    }
                });
            }
        },
        /**
         * Собирает значения всех полей на форме и возвращает в функцию submit.
         * @returns {{_csrf: string, form: {}}}
         */
        compileFields: function () {
            var result = {_csrf: $('meta[name="csrf-token"]').attr('content'), form: {}};

            for (var i = 0; i < this.fields.length; i++) {
                var fieldName = this.fields[i];
                result.form[fieldName] = $('#' + fieldName).val();
            }

            return result;
        },
        /**
         * Очищает все поля на форме (только input и textarea).
         */
        clearFields: function () {
            for (var i = 0; i < this.fields.length; i++) {
                var element = $('#' + this.fields[i]);
                switch (element.prop('nodeName')) {
                    case 'INPUT':
                    case 'TEXTAREA':
                        element.val('');
                        break;
                }
            }
        },
        /**
         * Здесь можно выполнить какие-нибудь проверки и вернуть значение либо типа boolean, либо типа Object.
         * Если будет возвращено значение типа Object, то эти данные смерджется с массивом значений, который отправиться на сервер.
         * Если же boolean, то, в зависимости от значения, true или false, данные на сервер отправяться или нет.
         * @param data. массив значений полей
         * @returns {boolean|{}}
         */
        beforeSubmit: function (data) {
            return true;
        },
        /**
         * Скрывает все сообщения.
         */
        hideAllMessages: function () {
            $.each(this.containers, function (type, selector) {
                hideAlert($(selector));
            });
        },
        /**
         * Если this.action (событие) указано как 'create', тогда после ajax запроса выполниться данная фк-ция.
         * @param response. ответ сервера
         * @param data. массив значений полей
         */
        afterCreate: function (response, data) {
            this.afterSubmit(response, 'create');
        },
        /**
         * Если this.action (событие) указано как 'update', тогда после ajax запроса выполниться данная фк-ция.
         * @param response. ответ сервера
         * @param data. массив значений полей
         */
        afterUpdate: function (response, data) {
            this.afterSubmit(response, 'update');
        },
        /**
         * Данная фк-ция вызывается из фк-ций типа 'after' + название события
         * @param response. ответ сервера
         * @param action. название события 'create', 'update', ...
         */
        afterSubmit: function (response, action) {
            var alertType = 'success';

            switch (response.status) {
                case 1:
                    if (action === 'create') {
                        this.clearFields();
                    }
                    break;
                case 0:
                    alertType = 'error';
                    break;
                case 2:
                    if (action === 'create') {
                        this.clearFields();
                    }
                    alertType = 'warning';
                    break;
            }

            showAlert($(this.containers[alertType]), response.messages);
        }
    };

    return Form;
})();