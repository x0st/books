<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/24/2016
 * Time: 3:04 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class BootstrapAsset extends AssetBundle
{
	public $jsOptions = [
		'position' => \yii\web\View::POS_HEAD
	];
	
	public $js = [
		'bower/bootstrap/dist/js/bootstrap.js'
	];
	
	public $css = [
		'bower/bootstrap/dist/css/bootstrap.css'
	];
	
	public $depends = [
		'backend\assets\JqueryAsset'
	];
}