<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/28/2016
 * Time: 4:55 PM
 */

namespace backend\assets;


use yii\web\AssetBundle;

class LightBox2Asset extends AssetBundle
{
    public $js = [
        'bower/lightbox2/dist/js/lightbox.js'
    ];

    public $css = [
        'bower/lightbox2/dist/css/lightbox.css'
    ];
}