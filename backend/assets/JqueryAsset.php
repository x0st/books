<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/24/2016
 * Time: 3:33 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class JqueryAsset extends AssetBundle
{
	public $jsOptions = [
		'position' => \yii\web\View::POS_HEAD
	];
	
	public $js = [
		'bower/jquery/dist/jquery.js'
	];
}