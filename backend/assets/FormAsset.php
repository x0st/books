<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/24/2016
 * Time: 11:54 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class FormAsset extends AssetBundle
{
	public $jsOptions = [
		'position' => \yii\web\View::POS_HEAD
	];
	
	public $js = [
		'bower/avega-yii2-form/avega.yii2-form.js'
	];
}