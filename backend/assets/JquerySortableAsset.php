<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/28/2016
 * Time: 8:08 PM
 */

namespace backend\assets;


use yii\web\AssetBundle;

class JquerySortableAsset extends AssetBundle
{
    public $js = [
        'bower/jquery-sortable/source/js/jquery-sortable.js'
    ];
}