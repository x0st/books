<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/24/2016
 * Time: 3:37 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class ListTableAsset extends AssetBundle
{
	public $jsOptions = [
		'position' => \yii\web\View::POS_HEAD
	];
	
	public $css = [
		'bower/avega-yii2-list-table/avega.yii2-list-table.css'
	];
	
	public $js = [
		'bower/avega-yii2-list-table/avega.yii2-list-table.js'
	];
	
	public $depends = [
		'backend\assets\JqueryAsset'
	];
}