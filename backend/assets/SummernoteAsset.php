<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/27/2016
 * Time: 6:46 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class SummernoteAsset extends AssetBundle
{
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

    public $css = [
        'bower/summernote/dist/summernote.css'
    ];

    public $js = [
        'bower/summernote/dist/summernote.js'
    ];

    public $depends = [
        'backend\assets\BootstrapAsset',
        'backend\assets\JqueryAsset'
    ];
}