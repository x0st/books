<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/30/2016
 * Time: 8:06 PM
 * @var $content
 */
use yii\bootstrap\Html;

\backend\assets\BootstrapAsset::register($this);
\backend\assets\AdminLTEAsset::register($this);
\backend\assets\FontAwesomeAsset::register($this);
\backend\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body class="hold-transition login-page">
<?php $this->beginBody() ?>
<?= $content ?>
<input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
