<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;

\backend\assets\BootstrapAsset::register($this);
\backend\assets\AdminLTEAsset::register($this);
\backend\assets\FontAwesomeAsset::register($this);
\backend\assets\FormAsset::register($this);
\backend\assets\AppAsset::register($this);

$leftMenu = require(__DIR__ . '/../../config/left-menu.php');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script>
        window._csrf = $('meta[name=csrf-token]').attr('content');
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
    <header class="main-header">
        <a class="logo">
            <!-- буква П заглавная -->
            <span class="logo-mini"><?= iconv('CP1251', 'UTF8', chr(207)) ?></span>
            <span class="logo-lg"><?= Yii::$app->name ?></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a onmouseenter="$(this).css('text-decoration', 'none');" class="sidebar-toggle" data-toggle="offcanvas"
               role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <?php foreach ($leftMenu as $index => $item): ?>
                    <li class="treeview" tabindex="<?= $index ?>">
                        <a href="<?= Url::to([$item['url']]) ?>">
                            <i class="fa <?= $item['icon'] ?>"></i>
                            <span><?= $item['name'] ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        <?= $content ?>
    </div>
    <footer class="main-footer">
        <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
        <div class="pull-right hidden-xs"><b>Версия</b> <?= Yii::$app->version ?></div>
        <strong>Все права защищены © <?= date('Y') ?> <?= Yii::$app->params['poweredBy'] ?></strong>
    </footer>
    <div class="control-sidebar-bg"></div>
</div>
<div class="modal-ajax"></div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
