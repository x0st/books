<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/27/2016
 * Time: 4:33 PM
 * @var $books array
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>Книги</h3>
        </div>
        <div class="col-md-6 pull-right">
            <a href="<?= \yii\helpers\Url::to(['/books/new']) ?>">
                <button class="btn btn-warning pull-right">Добавить книгу</button>
            </a>
        </div>
    </div>
</div>
<?php \avega\Insets::getAlert(['success', 'error']) ?>
<div class="content">
    <div class="books">
        <?php foreach ($books as $i => $book): ?>
            <!-- открыть строку если книга - первая, или первая в СТРОКЕ -->
            <?php $openRow = ($i == 0) || ($i % 4 === 0) ?>
            <!-- закрыть строку если книга - последняя или последняя в строке -->
            <?php $closeRow = $i === count($books) - 1 || ($i != 1 && ($i + 1) % 4 === 0) ?>
            <?php if ($openRow)  : ?>
                <div class="row">
            <?php endif ?>
            <?php $thumbnailUrl = \Yii::$app->params['coverUrl'] . \backend\models\BookForm::THUMBNAIL_WIDTH . 'x' . \backend\models\BookForm::THUMBNAIL_HEIGHT . '_' . $book['cover'] ?>
            <div class="col-md-3">
                <div class="book-item">
                    <a href="<?= \yii\helpers\Url::to(['/books/sections/' . $book['id']]) ?>">
                        <i class="fa fa-book view-book" aria-hidden="true"></i>
                    </a>
                    <i class="fa fa-times delete-book" title="Удалить" data-id="<?= $book['id'] ?>"></i>
                    <a href="<?= \yii\helpers\Url::to(['/books/edit/' . $book['id']]) ?>"><i
                            class="fa fa-pencil edit-book" title="Редактировать"></i></a>
                    <h5 class="book-title"><?= $book['title'] ?></h5>
                    <img src="<?= $thumbnailUrl ?>" alt="">
                </div>
            </div>
            <?php if ($closeRow)  : ?>
                </div>
            <?php endif ?>
        <?php endforeach ?>
    </div>
</div>
<script>
    var body = $('body');

    var Book = {
        selectors: {
            /**
             * Кнопка удаления книги.
             */
            deleteBook: '.delete-book',
            /**
             * Контейнер со всеми книгами.
             */
            container: '.books'
        },

        handlers: {
            /**
             * Клик по кнопке удалить книгу.
             */
            deleteBook: function (event) {
                var id = $(this).data('id');

                event.data._this.deleteBook(id);
            }
        },
        /**
         * Функция спросить пользователя, хочет ли он удалить книгу.
         * @return {boolean}
         */
        beforeDeleteBook: function () {
            return confirm('Вы уверены что хотите удалить книгу? Вместе с ней будут удалены все главы и изображения.');
        },
        /**
         * Ajax запрос на удаление книги.
         * @param id
         * @return {boolean}
         */
        deleteBook: function (id) {
            var _this = this;

            if (!_this.beforeDeleteBook()) {
                return false;
            }

            $.ajax({
                url: '<?= \yii\helpers\Url::to(['/books/delete/']) ?>/' + id,
                type: 'POST',
                dataType: 'json',
                data: {_csrf: window._csrf},
                success: function (response) {
                    _this.afterDeleteBook(response, id);
                },
                error: function () {
                    alert('Ошибка удаления книги');
                }
            });
        },
        /**
         * Фк-ция выполниться после ajax запроса на удаление книги.
         * @param id
         * @param response
         */
        afterDeleteBook: function (response, id) {
            hideAlert();
            showAlert(response.status, response.messages);

            if (response.status == 1) {
                var row = $(this.selectors.deleteBook + '[data-id=' + id + ']').parent().parent().parent();

                if (row.find('.col-md-3').length == 1) {
                    row.remove();
                }else{
                    $(this.selectors.deleteBook + '[data-id=' + id + ']').parent().parent().remove();
                }

                this.reSortBooks();
            }
        },
        reSortBooks: function () {
            // клониурем массив со всеми книгами
            var books = $(this.selectors.container).find('.col-md-3').clone();
            $(this.selectors.container).html('');

            // здесь будет html код книг
            var html = '';

            $.each(books, function (index, book) {
                var openRow = index == 0 || index % 4 == 0;
                var closeRow = index == (books.length - 1) || (index != 1 && (index + 1) % 4 == 0);

                html += openRow ? '<div class="row">' : '';

                html += book.outerHTML;

                html += closeRow ? '</div>' : '';
            });

            // вставляем книги заново
            $(this.selectors.container).html(html);
        }
    };

    body.on('click', Book.selectors.deleteBook, {_this: Book}, Book.handlers.deleteBook);
</script>