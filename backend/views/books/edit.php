<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/27/2016
 * Time: 6:41 PM
 * @var $book
 * @var $this \yii\web\View
 */
\backend\assets\SummernoteAsset::register($this);
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-12 pull-left">
            <h3>Бла бла бла, название</h3>
        </div>
    </div>
</div>
<?php \avega\Insets::getAlert(['success', 'error']) ?>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Название</label>
                        <input type="text" id="title" class="form-control" value="<?= $book->title ?>">
                    </div>
                    <div class="form-group">
                        <input type="checkbox" id="cutFiles" value="<?= (int)$book->cutFiles ?>" <?= (bool)$book->cutFiles ? 'checked' : ''?>>
                        <label for="cutFiles">Разрезать каждый новый файл пополам</label>
                    </div>
                    <div class="row">
                        <div class="col-xs-9">
                            <div class="form-group">
                                <label>Обложка</label>
                                <input type="file" id="file" class="form-control" title="Обложка">
                            </div>
                        </div>
                        <div class="col-xs-3 pull-right">
                            <div class="form-group">
                                <button class="btn btn-info btn-md pull-right btn-upload-cover"
                                        style="margin-top: 24px;">Загрузить
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group progress-container" style="display: none;">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 0">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Описание</label>
                        <textarea id="description" class="form-control"><?= $book->description ?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="hidden" id="cover" value="<?= $book->cover ?>">
                        <button class="btn btn-warning btn-md btn-update">Сохранить</button>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
    </div>
</div>
<script>
    var progressContainer = $('.progress-container');
    var progressBar = $('.progress-bar');

    // записываем в значение чекбокса состояние чекбокса
    $('#cutFiles').change(function () {
        $(this).val($(this).prop('checked') ? 1 : 0);
    });

    // клик по кнопке загрузки изображения
    $('.btn-upload-cover').click(function () {
        var file = document.getElementById('file').files[0];

        if (typeof file != typeof undefined) {

            var fd = new FormData();

            fd.append('cover', file);
            fd.append('_csrf', window._csrf);

            progressContainer.show();
            progressBar.addClass('active');

            // отсылаем на сервер изображение
            $.ajax({
                url: '<?= \yii\helpers\Url::to(['/books/upload-cover']) ?>',
                data: fd,
                type: 'POST',
                dataType: 'json',
                contentType: false,
                processData: false,
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();

                    if (xhr.upload) {
                        xhr.upload.onprogress = function (_xhr) {
                            var pos = parseInt(100 * _xhr.loaded / _xhr.total);

                            progressBar.css({'width': pos + '%'});

                            if (pos == 100) {
                                progressBar.removeClass('active');
                            }
                        }
                    }

                    return xhr;
                },
                success: function (response) {
                    var alertType = 'error';

                    if (response.status == 1) {
                        alertType = 'success';
                        $('#cover').val(response.fileName);
                    }

                    showAlert(alertType, response.messages)
                }
            });
        }
    });

    $(function () {
        $('#description').summernote();

        // после запроса ajax'a обновляем заголовок страницы сверху
        Form.prototype.afterUpdate = function (response, data) {
            if (response.status == 1) {
                $('.content-header h3').text(data.title);
            }
            this.afterSubmit(response, 'update');
        };

        (new Form('book', {
            'action': 'update',
            'url': '<?= \yii\helpers\Url::to(['/books/save/' . $book->id]) ?>',
            'fields': ['description', 'cover', 'title', 'cutFiles']
        }));
    });
</script>