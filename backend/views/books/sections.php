<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/28/2016
 * Time: 12:46 PM
 * @var $book
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>Главы книги: <?= $book->title ?></h3>
        </div>
    </div>
</div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 btn-add-section-container">
                    <div class="form-group">
                        <button class="btn btn-success btn-add-section">
                            Добавить главу
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row sections">
                <?php foreach ($book->sections as $section): ?>
                    <div class="col-md-12 section-item" data-id="<?= $section->id ?>">
                        <i class="fa fa-list-alt" aria-hidden="true"></i>
                        <div class="section-item-name"><span><?= $section->name ?></span></div>
                        <a href="<?= \yii\helpers\Url::to(['/sections/view/' . $section->id]) ?>">
                            <i class="fa fa-eye view-section"></i>
                        </a>
                        <a href="#" class="delete-section-item">
                            <i class="fa fa-times delete-section"></i>
                        </a>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<script>
    var body = $('body');

    var Section = {
        selectors: {
            /**
             * @param item {string}
             */
            item: '.section-item',
            /**
             * @param name {string}
             */
            name: '.section-item-name span',
            /**
             * @param nameInput {string}
             */
            nameInput: '.section-item-name input',
            /**
             * @param deleteItem {string}
             */
            deleteItem: '.delete-section-item',
            /**
             * @param addItem {string}
             */
            addItem: '.btn-add-section',
            /**
             * @param container {string}
             */
            container: '.sections',
        },

        /**
         * Массив функций обработчиков событий.
         */
        handlers: {
            /**
             * Обработчик клика по названию главы.
             * @param event
             */
            name: function (event) {
                var currentName = $(this).text();
                var input = $('<input type="text" value="' + currentName + '">');

                $(this).parent().data('name', currentName);
                $(this).parent().html(input);

                input.focus();
            },
            /**
             * Обработчик нажатия клавиши в инпуте редактирования главы.
             * @param event
             */
            editName: function (event) {
                if (event.keyCode === 13) {
                    var newName = $(this).val();
                    var oldName = $(this).parent().data('name');
                    var id = $(this).parent().parent().data('id');

                    if (!isEmpty(newName)) {
                        if (newName !== oldName) {
                            event.data._this.updateName(id, newName);
                        }

                        $(this).parent().html('<span>' + newName + '</span>');
                    }
                }
            },
            /**
             * Обработчик клика по кнопке "удалить главу".
             */
            deleteSection: function (event) {
                var id = $(this).parent().data('id');

                event.data._this.deleteSection(id);
            },
            /**
             * Обработчик клика по кнопке "создать дефолтную секцию".
             */
            addSection: function (event) {
                event.data._this.createSection();
            }
        },
        /**
         * Функция выполниться после запроса об обновлении имени.
         * @param response
         * @param id
         */
        afterUpdateName: function (response, id) {
            var input = $(this.selectors.item + '[data-id=' + id + ']').find(this.selectors.nameInput);
            var name = '';

            if (response.status == 1) {
                name = input.val();
                input.parent().html('<span>' + name + '</span>');
            } else {
                name = input.parent().data('name');
                alert('Ошибка обновления имени.');
            }

            input.parent().html('<span>' + name + '</span>');
        },
        /**
         * Ajax запрос на обновление имени.
         * @param id
         * @param name
         */
        updateName: function (id, name) {
            var _this = this;

            $.ajax({
                url: '<?= \yii\helpers\Url::to(['/sections/update-section-name/']) ?>/' + id,
                type: 'POST',
                dataType: 'json',
                data: {name: name, _csrf: window._csrf},
                success: function (response) {
                    _this.afterUpdateName(response, id);
                },
                error: function () {
                    _this.afterUpdateName({status: 0}, id);
                }
            });
        },
        /**
         * Функция выполниться перед ajax запросом удаления главы.
         * @return boolean
         */
        beforeDeleteSection: function () {
            return confirm('Вы уверены, что хотите удалить главу? Вместе с главой удаляться все изображения внутри.');
        },
        /**
         * Функция удаляет главу.
         * @param id
         * @return {boolean}
         */
        deleteSection: function (id) {

            if (!this.beforeDeleteSection()) {
                return false;
            }

            var _this = this;

            $.ajax({
                url: '<?= \yii\helpers\Url::to(['/sections/delete-section/']) ?>/' + id,
                type: 'POST',
                data: {_csrf: window._csrf},
                dataType: 'json',
                success: function (response) {
                    _this.afterDeleteSection(response, id);
                },
                error: function () {
                    _this.afterDeleteSection({status: 0}, id);
                }
            });
        },
        /**
         * Ajax запрос на создание главы со стандартным именем.
         */
        createSection: function () {
            var _this = this;

            $.ajax({
                url: '<?= \yii\helpers\Url::to(['/sections/create-section/' . $book->id]) ?>',
                type: 'POST',
                data: {_csrf: window._csrf},
                dataType: 'json',
                success: function (response) {
                    _this.afterCreateSection(response);
                },
                error: function () {
                    _this.afterCreateSection({status: 0});
                }
            });
        },
        /**
         * Функция выполниться после ajax запроса на создание главы.
         */
        afterCreateSection: function (response) {
            if (response.status == 1) {
                $(this.selectors.container).append(this.generateSectionItem(response.id));
            } else {
                alert('Ошибка добавления главы.');
            }
        },
        /**
         * Функция возвращает html код для главы.
         * @param id
         * @return string
         */
        generateSectionItem: function (id) {
            return '<div class="col-md-12 section-item" data-id="' + id + '">' +
                '<i class="fa fa-list-alt" aria-hidden="true"></i> ' +
                '<div class="section-item-name"><span><?= \common\models\Section::DEFAULT_NAME ?></span></div>' +
                '<a href="<?= \yii\helpers\Url::to(['/sections/view']) ?>/' + id + '"> ' +
                '<i class="fa fa-eye view-section"></i>' +
                '</a> ' +
                '<a href="#" class="delete-section-item"><i class="fa fa-times delete-section"></i></a>' +
                '</div>';
        },
        /**
         * После ajax запроса удаления главы вызовется данная фк-ция.
         * @param response
         * @param id
         */
        afterDeleteSection: function (response, id) {
            if (response.status == 1) {
                $(this.selectors.item + '[data-id=' + id + ']').remove();
            } else {
                alert('Ошибка удаления главы.');
            }
        }
    };

    body.on('click', Section.selectors.name, {_this: Section}, Section.handlers.name);
    body.on('keyup', Section.selectors.nameInput, {_this: Section}, Section.handlers.editName);
    body.on('click', Section.selectors.deleteItem, {_this: Section}, Section.handlers.deleteSection);
    body.on('click', Section.selectors.addItem, {_this: Section}, Section.handlers.addSection);
</script>