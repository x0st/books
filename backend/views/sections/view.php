<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/28/2016
 * Time: 3:11 PM
 * @var $this \yii\web\View
 * @var $section \common\models\Section
 * @var $book \common\models\Book
 */
\backend\assets\LightBox2Asset::register($this);
\backend\assets\JquerySortableAsset::register($this);

$thumbnailWidth = \backend\models\ImagesForm::THUMBNAIL_WIDTH;
$thumbnailHeight = \backend\models\ImagesForm::THUMBNAIL_HEIGHT;
$thumbnailPrefix = $thumbnailWidth . 'x' . $thumbnailHeight . '_';
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-12">
            <h3>Глава: <?= $section->name ?></h3>
        </div>
    </div>
</div>
<?php \avega\Insets::getAlert(['success', 'error', 'warning']) ?>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="files">Выберите файлы</label>
                        <input type="file" id="files" multiple>
                    </div>
                    <?php if ($book['cutFiles']): ?>
                        <div class="form-group">
                            <label>
                                Резать изображения пополам (<?= \backend\models\ImagesForm::IMAGE_WIDTH ?>
                                x<?= \backend\models\ImagesForm::IMAGE_HEIGHT ?>)
                                <i class="fa fa-check" style="color: #00aa00"></i>
                            </label>
                        </div>
                    <?php endif ?>
                    <div class="form-group progress-container" style="display: none;">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 0">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-info btn-upload">Загрузить</button>
                        <button class="btn btn-warning btn-update">Сохранить</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Передвинуть миниатюру вправо</label>
                        <div>
                            <img src="<?= \yii\helpers\Url::to(['/images/arrow-right.png']) ?>" width="30" alt="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Сместить выделение вправо</label>
                        <div>
                            <img src="<?= \yii\helpers\Url::to(['/images/shift.png']) ?>" width="87" alt="">
                            <img src="<?= \yii\helpers\Url::to(['/images/arrow-right.png']) ?>" width="30" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="thumbnails-container col-md-12">
            <?php foreach ($section->images as $i => $image): ?>

                <?php $openRow = ($i == 0) || ($i % 4 === 0) ?>

                <?php $closeRow = $i === count($section->images) - 1 || ($i != 1 && ($i + 1) % 4 === 0) ?>
                <?php if ($openRow)  : ?>
                    <div class="row">
                <?php endif ?>
                <div class="col-xs-3" data-id="<?= $image->id ?>" data-name="<?= $image->file ?>">
                    <div class="thumbnail-item">
                        <a data-lightbox="thumbnails" href="<?= Yii::$app->params['paperUrl'] ?><?= $image->file ?>">
                            <img src="<?= Yii::$app->params['paperUrl'] ?><?= $thumbnailPrefix ?><?= $image->file ?>"
                                 alt=""
                                 class="<?= $i == 0 ? 'selected-thumbnail' : '' ?>">
                        </a>
                        <i class="fa fa-times delete-thumbnail"></i>
                    </div>
                </div>
                <?php if ($closeRow)  : ?>
                    </div>
                <?php endif ?>
            <?php endforeach ?>
        </div>
    </div>
</div>

<script>
    var body = $('body');

    var Thumbnail = {
        selectors: {
            thumbnailsContainer: '.thumbnails-container',
            moveLeft: '.move-left-thumbnail',
            moveRight: '.move-right-thumbnail',
            selectedThumbnail: '.selected-thumbnail',
            deleteImage: '.delete-thumbnail'
        },

        handlers: {
            deleteImage: function (event) {
                var id = $(this).parent().parent().data('id');
                var name = $(this).parent().parent().data('name');

                event.data._this.deleteImage(id, isEmpty(name) ? null : name);
            }
        },
        /**
         * Перемещает миниатюру влево на одну позицию.
         */
        moveLeft: function () {
            var currentThumbnail = $(this.selectors.selectedThumbnail).parent().parent().parent();
            var currentRow = currentThumbnail.parent();

            this.unselectAllThumbnails();
            this.markAsSelected(currentThumbnail.find('img'));

            // если предыдущий элемент - миниатюра, тогда меняем местами
            if (currentThumbnail.prev().hasClass('col-xs-3')) {
                var prevThumbnail = currentThumbnail.prev();
                prevThumbnail.before(currentThumbnail);

            } else // предыдущего элемента не существует, но существует предыдущая строка с миниатюрами
            if (typeof currentThumbnail.prev()[0] === typeof undefined && currentRow.prev().hasClass('row')) {

                var prevRow = currentRow.prev();
                var prevRowFirstThumbnail = $(prevRow.children()[prevRow.children().length - 1]);

                // клонируем объекты, чтобы потом ними заменить реальные элементы
                var currentThumbnailClone = currentThumbnail.clone();
                var prevRowFirstThumbnailClone = $(prevRow.children()[prevRow.children().length - 1]).clone();

                currentThumbnail.replaceWith(prevRowFirstThumbnailClone);
                prevRowFirstThumbnail.replaceWith(currentThumbnailClone);
            }
        },
        /**
         * Перемещает миниатюру вправо на одну позицию.
         */
        moveRight: function () {
            var currentThumbnail = $(this.selectors.selectedThumbnail).parent().parent().parent();
            var currentRow = currentThumbnail.parent();

            this.unselectAllThumbnails();
            this.markAsSelected(currentThumbnail.find('img'));

            // если следующий элемент - миниатюра, тогда меняем местами
            if (currentThumbnail.next().hasClass('col-xs-3')) {
                var nextThumbnail = currentThumbnail.next();
                nextThumbnail.after(currentThumbnail);

            } else // следующего элемента не существует, но существует следующая строка с миниатюрами
            if (typeof currentThumbnail.next()[0] === typeof undefined && currentRow.next().hasClass('row')) {

                var nextRow = currentRow.next();
                var nextRowFirstThumbnail = $(nextRow.children()[0]);

                // клонируем объекты, чтобы потом ними заменить реальные элементы
                var nextRowFirstThumbnailClone = $(nextRow.children()[0]).clone();
                var currentThumbnailClone = currentThumbnail.clone();

                currentThumbnail.replaceWith(nextRowFirstThumbnailClone);
                nextRowFirstThumbnail.replaceWith(currentThumbnailClone);
            }
        },
        /**
         * Помечает миниатюру как активную.
         * @param imgObject {{}}
         */
        markAsSelected: function (imgObject) {
            $(imgObject).addClass('selected-thumbnail');
        },
        /**
         * Снимает выделение со всех миниатюр.
         */
        unselectAllThumbnails: function () {
            $(this.selectors.thumbnailsContainer).find('img').removeClass('selected-thumbnail');
        },
        /**
         * Помечает следующую миниатюра как выделенную.
         */
        selectNextThumbnail: function () {
            if (!$.isEmptyObject($(Thumbnail.selectors.selectedThumbnail))) {

                var currentThumbnail = $(Thumbnail.selectors.selectedThumbnail).parent().parent().parent();
                var currentRow = currentThumbnail.parent();

                // если следующий элемент - миниатюра, тогда помечаем её
                if (currentThumbnail.next().hasClass('col-xs-3')) {
                    var nextThumbnail = currentThumbnail.next();

                    this.unselectAllThumbnails();

                    nextThumbnail.find('img').addClass('selected-thumbnail');
                } else // следующего элемента не существует, но существует следующая строка с миниатюрами
                if (typeof currentThumbnail.next()[0] === typeof undefined && currentRow.next().hasClass('row')) {

                    var nextRow = currentRow.next();
                    var nextRowFirstThumbnail = $(nextRow.children()[0]);

                    this.unselectAllThumbnails();

                    nextRowFirstThumbnail.find('img').addClass('selected-thumbnail');
                }
            }
        },
        /**
         * Помечает предыдущую миниатюра как выделенную.
         */
        selectPrevThumbnail: function () {
            if (!$.isEmptyObject($(Thumbnail.selectors.selectedThumbnail))) {

                var currentThumbnail = $(Thumbnail.selectors.selectedThumbnail).parent().parent().parent();
                var currentRow = currentThumbnail.parent();

                // если предыдущий элемент - миниатюра, тогда помечаем её
                if (currentThumbnail.prev().hasClass('col-xs-3')) {
                    var nextThumbnail = currentThumbnail.prev();

                    this.unselectAllThumbnails();

                    nextThumbnail.find('img').addClass('selected-thumbnail');
                } else // предыдущего элемента не существует, но существует предыдущая строка с миниатюрами
                if (typeof currentThumbnail.prev()[0] === typeof undefined && currentRow.prev().hasClass('row')) {

                    var prevRow = currentRow.prev();
                    var prevRowFirstThumbnail = $(prevRow.children()[prevRow.children().length - 1]);

                    this.unselectAllThumbnails();

                    prevRowFirstThumbnail.find('img').addClass('selected-thumbnail');
                }
            }
        },
        /**
         * Ajax запрос на удаления изображения.
         * @param id
         * @param name
         */
        deleteImage: function (id, name) {
            var _this = this;
            $.ajax({
                url: '<?= \yii\helpers\Url::to(['/sections/delete-image/' . $section->id]) ?>',
                type: 'POST',
                data: {id: id, name: name, _csrf: window._csrf},
                dataType: 'json',
                success: function (response) {
                    _this.afterDeleteImage(response, id, name);
                },
                error: function () {
                    alert('Ошибка удаления изображения.');
                }
            });
        },
        /**
         * Фк-ция выполниться после ajax запроса на удаление изображения.
         * @param response
         * @param id
         * @param name
         */
        afterDeleteImage: function (response, id, name) {
            hideAlert();
            showAlert(response.status, response.messages);

            if (response.status == 1) {
                // определяем по какому селектору мы будет удалять миниатюру
                var selector = isEmpty(name) ? '.col-xs-3[data-id="' + id + '"]' : '.col-xs-3[data-name="' + name + '"]';
                var thumbnail = $(this.selectors.thumbnailsContainer).find(selector);

                // если миниатюра является последней в строке, тогда удаляем строку
                if (thumbnail.parent().find('.col-xs-3').length == 1) {
                    thumbnail.parent().remove();
                } else { // иначе удаляем миниатюру
                    thumbnail.remove();
                }

                this.reSortThumbnails();
            }
        },
        /**
         * Пересортировует все миниатюры.
         */
        reSortThumbnails: function () {
            // клониурем массив со всеми миниатюрами
            var thumbnails = $(this.selectors.thumbnailsContainer).find('.col-xs-3').clone();
            $(this.selectors.thumbnailsContainer).html('');

            // здесь будет html код миниатюр
            var html = '';

            $.each(thumbnails, function (index, thumbnail) {
                var openRow = index == 0 || index % 4 == 0;
                var closeRow = index == (thumbnails.length - 1) || (index != 1 && (index + 1) % 4 == 0);

                html += openRow ? '<div class="row">' : '';

                html += thumbnail.outerHTML;

                html += closeRow ? '</div>' : '';
            });

            // вставляем миниатюры заново
            $(this.selectors.thumbnailsContainer).html(html);
        },
        /**
         * Возвращает массив изображений в таком порядке, в какой они выстроены.
         */
        compileImages: function () {
            var result = [];

            $.each($(this.selectors.thumbnailsContainer).find('.col-xs-3'), function () {
                var name = $(this).data('name');

                result.push(name);
            });

            return result;
        }
    };

    var UploadImage = {
        selectors: {
            buttonUpload: '.btn-upload',
            progressBarContainer: '.progress-container',
            progressBar: '.progress-bar',
            thumbnailsContainer: '.thumbnails-container'
        },

        handlers: {
            /**
             * Клик по кнопке "загрузить".
             */
            uploadImages: function (event) {
                var input = document.getElementById('files');
                var files = input.files;

                if (files.length) {
                    var data = new FormData;

                    $.each(files, function (index, file) {
                        data.append('images[]', file);
                    });

                    event.data._this.uploadImages(data);
                }
            }
        },
        /**
         * Ajax запрос на загрузку изображений.
         * @param data FormData
         */
        uploadImages: function (data) {
            var _this = this;

            data.append('_csrf', window._csrf);

            $(_this.selectors.progressBarContainer).show();
            $(_this.selectors.progressBar).addClass('active');
            $(_this.selectors.progressBar).css({'width': '0'});

            $.ajax({
                url: '<?= \yii\helpers\Url::to(['/sections/upload-images/' . $section->id])?>',
                type: 'POST',
                dataType: 'json',
                contentType: false,
                processData: false,
                data: data,
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();

                    if (xhr.upload) {
                        xhr.upload.onprogress = function (_xhr) {
                            var pos = parseInt(100 * _xhr.loaded / _xhr.total);

                            $(_this.selectors.progressBar).css({'width': pos + '%'});

                            if (pos == 100) {
                                $(_this.selectors.progressBar).removeClass('active');

                                setTimeout(function () {
                                    $(_this.selectors.progressBar).css({'width': '0'});
                                }, 3000);
                            }
                        }
                    }

                    return xhr;
                },
                success: function (response) {
                    _this.afterUploadImages(response);
                },
                error: function () {
                    alert('Ошибка загрузки изображений.');
                }
            });
        },
        /**
         * Функция выполниться после ajax запроса на загрузку изображений.
         */
        afterUploadImages: function (response) {
            hideAlert();

            if (response.status != 0) {
                // если нет ниодного выделенного изображения, тогда выделяем самое первое
                if ($(Thumbnail.selectors.selectedThumbnail).length === 0) {
                    this.displayUploadedImages(response.savedFiles);
                    $($(Thumbnail.selectors.thumbnailsContainer).find('img')[0]).addClass(Thumbnail.selectors.selectedThumbnail.substring(1));
                } else {
                    this.displayUploadedImages(response.savedFiles);
                }
            }

            showAlert(response.status, response.messages);
        },
        /**
         * Отображает на страницу загруженные изображения.
         * @param images {Array}
         */
        displayUploadedImages: function (images) {
            var i = 0;
            // если на странице уже имеются какие либо миниатюры
            if ($(this.selectors.thumbnailsContainer).find('.row').length > 0) {
                // получаем последнюю строку, где хранятся миниатюры
                var lastRow = $(this.selectors.thumbnailsContainer).find('.row').last();

                // если в последней строке кол-во миниатюр меньше чем 4, т.е.
                // строка не является до конца заполненной
                if (lastRow.find('.col-xs-3').length != 4) {
                    // перебираем все изображения, что были сохранены
                    for (i = 0; i < images.length; i++) {
                        // добавляем в строку миниатюру
                        $(lastRow).append(this.generateThumbnail(images[i], false, false));
                        // удаляем изображение из массива изображений
                        images.splice(i, 1);
                        // снова проверяем, заполнена ли строка
                        if (lastRow.find('.col-xs-3').length == 4) {
                            break;
                        }
                    }
                }
            }

            var html = '';

            for (i = 0; i < images.length; i++) {
                var openRow = i == 0 || i % 4 == 0;
                var closeRow = i == (images.length - 1) || (i != 1 && (i + 1) % 4 == 0);
                html += this.generateThumbnail(images[i], openRow, closeRow);
            }

            $(this.selectors.thumbnailsContainer).append(html);
            Thumbnail.reSortThumbnails();
        },
        /**
         * Возвращает html код для миниатюры.
         */
        generateThumbnail: function (fileName, openRow, closeRow) {
            return (openRow ? '<div class="row">' : '') + '\
                <div class="col-xs-3" data-id="" data-name="' + fileName + '">\
                <div class="thumbnail-item">\
                <a data-lightbox="thumbnails" href="<?= Yii::$app->params['paperUrl'] ?>/' + fileName + '">\
                <img src="<?= Yii::$app->params['paperUrl'] ?>/<?= $thumbnailPrefix ?>' + fileName + '" alt="">\
                </a>\
                <i class="fa fa-times delete-thumbnail"></i>\
                </div>\
                </div>' +
                (closeRow ? '</div>' : '');
        }
    };

    body.delegate(Thumbnail.selectors.deleteImage, 'click', {_this: Thumbnail}, Thumbnail.handlers.deleteImage);

    body.on('keyup', function (event) {
        if (event.shiftKey) {
            switch (event.keyCode) {
                // выделить следующую миниатюру
                case 39: // стрелка вправо
                    Thumbnail.selectNextThumbnail();
                    break;
                // выделить предыдущую миниатюру
                case 37: // стрелка влево
                    Thumbnail.selectPrevThumbnail();
                    break;
            }
        } else {
            switch (event.keyCode) {
                // передвинуть миниатюру влево
                case 37: // стрелка влево
                    Thumbnail.moveLeft();
                    break;
                // передвинуть миниатюру вправо
                case 39: // стрелка вправо
                    Thumbnail.moveRight();
                    break;
                // снять выделение
                case 27: // esc
//                    Thumbnail.unselectAllThumbnails();
                    break;
            }
        }
    });

    body.on('click', UploadImage.selectors.buttonUpload, {_this: UploadImage}, UploadImage.handlers.uploadImages);

    $(function () {
        Form.prototype.beforeSubmit = function () {
            return Thumbnail.compileImages();
        };

        new Form('update', {
            action: 'update',
            url: '<?= \yii\helpers\Url::to(['/sections/update-images/' . $section->id]) ?>'
        });
    });
</script>