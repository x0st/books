<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$loginError = Yii::$app->session->getFlash('LoginFormError', false);

?>
<div class="login-box">
    <div class="login-logo">
        <a>
            <b>Книги</b>
        </a>
    </div>
    <div class="login-box-body row">
        <?php if ($loginError): ?>
            <p class="login-box-msg" style="color:red;">
                <?= $loginError ?>
            </p>
        <?php else: ?>
            <p class="login-box-msg">
                Войдите, чтобы управлять книгами.
            </p>
        <?php endif ?>
        <form method="POST" action="<?= Url::to(['/login']) ?>">
            <div class="form-group has-feedback">
                <input tabindex="1" type="text" name="form[username]" class="form-control" placeholder="Логин"
                       autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" tabindex="2" name="form[password]" class="form-control" placeholder="Пароль">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-4">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <button type="submit" tabindex="3" class="btn btn-primary btn-block btn-flat btn-login">Войти
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>