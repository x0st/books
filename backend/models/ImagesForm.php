<?php
/**
 * Файл для загрузкии сохранения изображений главы.
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/29/2016
 * Time: 6:14 PM
 */

namespace backend\models;

use avega\F;
use common\models\Section;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\base\Model;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * Class ImagesForm
 * @package backend\models
 * @property $images UploadedFile[]
 * @property $sectionId int
 */
class ImagesForm extends Model
{
    /**
     * Массив загруженных картинок.
     * @var $images UploadedFile[]
     */
    public $images;

    /**
     * id главы, куда загружать изображения.
     * @var $sectionId int
     */
    public $sectionId;

    /**
     * Имена сохраненных изображений.
     * @var array
     */
    private $savedFiles;

    /**
     * Ширина миниатюры.
     */
    const THUMBNAIL_WIDTH = 120;

    /**
     * Высота миниатюры.
     */
    const THUMBNAIL_HEIGHT = 120;

    /**
     * Ширина изображения, в случае если его нужно порезать пополам.
     */
    const IMAGE_WIDTH = 3622;
    /**
     * Высота изображения, в случае если его нужно порезать пополам.
     */
    const IMAGE_HEIGHT = 2205;
    /**
     * Уменшить большое изображение до таких размеров.
     */
    const SCALE_WIDTH = 903;
    const SCALE_HEIGHT = 1100;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'images', 'file', 'extensions' => 'png,jpg,jpeg', 'skipOnEmpty' => false, 'checkExtensionByMimeType' => false, 'maxFiles' => 0
            ],
        ];
    }

    /**
     * Сохраняет загруженные изображения и возвращает массив имен загруженных файлов.
     * @return array|boolean
     */
    private function saveImages()
    {
        $book = Section::findBookBySectionId($this->sectionId);
        // резать ли каждое изображение пополам
        $cutFiles = (bool)$book['cutFiles'];

        // массив имен сохраненных файлов
        $savedFiles = [];

        ini_set('memory_limit', '-1');

        /**
         * @var $image UploadedFile
         */
        foreach ($this->images as $image) {

            // не нужно резать файл пополам
            if (!$cutFiles) {
                $fileName = time() + mt_rand(-1000, 1000) . '_' . $image->name;
                // имя для миниатюры
                $thumbnailName = self::THUMBNAIL_WIDTH . 'x' . self::THUMBNAIL_HEIGHT . '_' . $fileName;

                $filePath = \Yii::getAlias(\Yii::$app->params['paperPath'] . $fileName);
                $thumbnailPath = \Yii::getAlias(\Yii::$app->params['paperPath'] . $thumbnailName);

                // пытаемся сохранить полное изображение и миниатюру
                $save = Image::thumbnail($image->tempName, self::SCALE_WIDTH, self::IMAGE_HEIGHT)
                        ->save($filePath) && Image::thumbnail($filePath, self::THUMBNAIL_WIDTH, self::THUMBNAIL_HEIGHT)
                        ->save($thumbnailPath);

                if ($save) {
                    // данный массив будет вставлен в бд
                    $savedFiles[] = ['file' => $fileName, 'sectionId' => $this->sectionId];
                    // данный массив будет отдан методу контроллена
                    $this->savedFiles[] = $fileName;
                }
            } else {
                // получаем размеры изображения
                list($imageWidth, $imageHeight) = getimagesize($image->tempName);
                // чтобы не было исключений при обрезке, сразу проверяем размеры изображения
                if ($imageHeight < self::IMAGE_HEIGHT || $imageWidth < self::IMAGE_WIDTH) {
                    continue;
                }

                // имена первого и второго файла
                $firstFileName = time() + mt_rand(-1000, 1000) . '_' . $image->name;
                $secondFileName = time() + mt_rand(-1000, 1000) . '_1' . $image->name;

                $firstThumbnailName = self::THUMBNAIL_WIDTH . 'x' . self::THUMBNAIL_HEIGHT . '_' . $firstFileName;
                $secondThumbnailName = self::THUMBNAIL_WIDTH . 'x' . self::THUMBNAIL_HEIGHT . '_' . $secondFileName;

                $firstFilePath = \Yii::getAlias(\Yii::$app->params['paperPath'] . $firstFileName);
                $secondFilePath = \Yii::getAlias(\Yii::$app->params['paperPath'] . $secondFileName);

                $firstThumbnailPath = \Yii::getAlias(\Yii::$app->params['paperPath'] . $firstThumbnailName);
                $secondThumbnailPath = \Yii::getAlias(\Yii::$app->params['paperPath'] . $secondThumbnailName);

                // режем первую часть
                $first = Image::crop($image->tempName, (int)(self::IMAGE_WIDTH / 2), self::IMAGE_HEIGHT)
                    ->thumbnail(new Box(self::SCALE_WIDTH, self::IMAGE_HEIGHT))
                    ->save($firstFilePath);
                // режем вторую часть
                $second = Image::crop($image->tempName, (int)(self::IMAGE_WIDTH / 2), self::IMAGE_HEIGHT, [
                    (int)(self::IMAGE_WIDTH / 2), 0
                ])
                    ->thumbnail(new Box(self::SCALE_WIDTH, self::IMAGE_HEIGHT))
                    ->save($secondFilePath);


                // первая миниатюра
                $firstThumbnail = Image::thumbnail($firstFilePath, self::THUMBNAIL_WIDTH, self::THUMBNAIL_HEIGHT)
                    ->save($firstThumbnailPath);
                // вторая миниатюра
                $secondThumbnail = Image::thumbnail($secondFilePath, self::THUMBNAIL_WIDTH, self::THUMBNAIL_HEIGHT)
                    ->save($secondThumbnailPath);

                if ($first && $second && $firstThumbnail && $secondThumbnail) {

                    $savedFiles[] = ['file' => $firstFileName, 'sectionId' => $this->sectionId];
                    $savedFiles[] = ['file' => $secondFileName, 'sectionId' => $this->sectionId];
                    $this->savedFiles[] = $firstFileName;
                    $this->savedFiles[] = $secondFileName;
                }
            }
        }

        return $savedFiles;
    }

    /**
     * Вставляет сохраненные изображения в бд.
     * @return bool|int. false если вставленно 0 изображений, true если вставлены все изображения, 0 если не все.
     */
    public function setImages()
    {
        $images = $this->saveImages();

        $imagesQuantity = count($images);

        if ($imagesQuantity == 0) {
            return false;
        }

        $query = \Yii::$app->db->createCommand()
            ->batchInsert(\common\models\Image::tableName(), [
                'file', 'sectionId'
            ], $images)
            ->execute();

        // кол-во затронутых строк после запроса
        $affectedRowsCount = $query;

        return $affectedRowsCount == 0 ? false : ($affectedRowsCount == $imagesQuantity ? true : 0);
    }

    /**
     * Возвращает массив имен сохраненных изображений.
     * @return array
     */
    public function getSavedFiles()
    {
        return $this->savedFiles;
    }
}