<?php
/**
 * Файл для создания/редактирования книги, загрузки обложки книги.
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/27/2016
 * Time: 8:15 PM
 */

namespace backend\models;

use avega\F;
use common\models\Book;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * Class BookForm
 * @package backend\models
 * @property $cover string|UploadedFile
 * @property $title string
 * @property $description string
 * @property $cutFiles boolean
 */
class BookForm extends Book
{
    const SCENARIO_EDIT = 'edit';
    const SCENARIO_CREATE = 'create';
    /**
     * Для загрузки обложки.
     */
    const SCENARIO_COVER = 'cover';

    /**
     * Ширина миниатюры.
     */
    const THUMBNAIL_WIDTH = 120;
    /**
     * Высота миниатюры.
     */
    const THUMBNAIL_HEIGHT = 120;

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'cover' => 'Обложка',
            'description' => 'Описание',
            'cutFiles' => 'Разрезать каждый новый файл пополам'
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_COVER => ['cover'],
            self::SCENARIO_EDIT => ['cover', 'title', 'description', 'cutFiles'],
            self::SCENARIO_CREATE => ['cover', 'title', 'description', 'cutFiles']
        ];
    }

    /**
     * Перед сохранением книги, удяляем её старую обложку, если загрузили новую.
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->cover != $this->getOldAttribute('cover')) {
            $this->deleteCurrentCover();
        }

        return parent::beforeSave($insert);
    }

    /**
     * Сохраняет обложку книги.
     * @return bool
     */
    public function uploadCover()
    {
        $fileName = time() + mt_rand(-1000, 1000) . '_' . $this->cover->name;
        // имя для миниатюры
        $thumbnailName = self::THUMBNAIL_WIDTH . 'x' . self::THUMBNAIL_HEIGHT . '_' . $fileName;

        $filePath = \Yii::getAlias(\Yii::$app->params['coverPath'] . $fileName);
        $thumbnailPath = \Yii::getAlias(\Yii::$app->params['coverPath'] . $thumbnailName);

        // пытаемся сразу сохранить обложку,
        // сделать из неё миниатюру и тоже сохранить
        $save = $this->cover->saveAs($filePath) &&
            Image::thumbnail($filePath, self::THUMBNAIL_WIDTH, self::THUMBNAIL_HEIGHT)->save($thumbnailPath);

        return $save ? $fileName : false;
    }

    /**
     * Проверяет, существует ли файл обложки.
     * @param $attribute
     */
    public function checkCover($attribute)
    {
        if (self::getScenario() !== self::SCENARIO_COVER) {
            if (!is_file(\Yii::getAlias(\Yii::$app->params['coverPath'] . $this->cover))) {
                $this->addError($attribute, 'Файл обложки не существует.');
            }
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['title', 'cover', 'description', 'cutFiles'],
                'required',
                'on' => [self::SCENARIO_EDIT, self::SCENARIO_CREATE]
            ],
            ['cover', 'checkCover'],
            ['cover', 'safe', 'on' => self::SCENARIO_COVER],
            [
                ['cover'],
                'file',
                'extensions' => 'png,jpg,jpeg',
                'skipOnEmpty' => true,
                'checkExtensionByMimeType' => false,
                'on' => self::SCENARIO_COVER
            ]
        ];
    }
}