<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/27/2016
 * Time: 4:09 PM
 */

return [
    [
        'name' => 'Книги',
        'url' => '/books',
        'icon' => 'fa-book'
    ],
    [
        'name' => 'Выход',
        'url' => '/logout',
        'icon' => 'fa-sign-out'
    ]
];