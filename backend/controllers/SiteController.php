<?php
namespace backend\controllers;

use avega\F;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function beforeAction($action)
    {
        $this->layout = 'guest';

        return parent::beforeAction($action);
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    /**
     * Login action.
     * @return string
     */
    public function actionLogin()
    {
        if (Yii::$app->request->isPost) {
            $model = new LoginForm();

            if ($model->load(Yii::$app->request->post(), 'form') && $model->login()) {
                return $this->redirect(\yii\helpers\Url::to(['/books']));
            } elseif ($model->hasErrors()) {
                Yii::$app->session->setFlash('LoginFormError', 'Неверный логин или пароль.');
            }

            return $this->redirect(\yii\helpers\Url::to(['/login']));
        }

        return $this->render('login');
    }

    /**
     * Logout action.
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
