<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 8/1/2016
 * Time: 12:59 PM
 */

namespace backend\controllers;

use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ImageController extends Controller
{
    /**
     *
     */
    public function actionGetImage($fileName)
    {
        $imagePath = \Yii::getAlias(\Yii::$app->params['paperPath'] . $fileName);

        if (file_exists($imagePath)) {
            $response = \Yii::$app->getResponse();
            $response->headers->set('Content-Type', 'image/jpeg');
            $response->format = Response::FORMAT_RAW;

            if (!is_resource($response->stream = fopen($imagePath, 'r'))) {
                throw new ForbiddenHttpException;
            }

            return $response->send();
        } else {
            throw new NotFoundHttpException;
        }
    }
}