<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/27/2016
 * Time: 4:33 PM
 */

namespace backend\controllers;


use avega\F;
use backend\models\BookForm;
use common\models\Book;
use common\models\Section;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class BooksController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Страница с книгами.
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'books' => Book::find()->orderBy('id DESC')->asArray()->all()
        ]);
    }

    /**
     * Страница редактировании книги.
     */
    public function actionEdit($id)
    {
        $book = Book::findOne($id);

        if ($book !== null) {
            return $this->render('edit', ['book' => $book]);
        } else {
            throw new NotFoundHttpException;
        }
    }

    /**
     * Страница создания новой книги.
     */
    public function actionNew()
    {
        return $this->render('new');
    }

    /**
     * Загрузка обложки (ajax).
     */
    public function actionUploadCover()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'status' => 0,
            'messages' => []
        ];

        $model = new BookForm(['scenario' => BookForm::SCENARIO_COVER]);
        $model->setAttribute('cover', UploadedFile::getInstanceByName('cover'));

        if ($model->validate()) {
            $uploadCover = $model->uploadCover();

            if ($uploadCover) {
                $response['status'] = 1;
                $response['messages'] = [
                    'Обложка загружена успешно.',
                    'Обложка будет обновлена после сохранения.'
                ];
                $response['fileName'] = $uploadCover;
            }
        } elseif ($model->hasErrors()) {
            $response['messages'] = $model->errors;
        } else {
            throw new BadRequestHttpException;
        }

        return $response;
    }

    /**
     * Обновляет книгу по id или создает новую, если id не указан (ajax).
     */
    public function actionSave($id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'status' => 0,
            'messages' => []
        ];

        if ($id !== null) {
            $model = BookForm::findOne($id);
            $model->setScenario(BookForm::SCENARIO_EDIT);
        } else {
            $model = new BookForm(['scenario' => BookForm::SCENARIO_CREATE]);
        }

        if ($model->load(\Yii::$app->request->post(), 'form') && $model->save()) {
            $response['status'] = 1;
            $response['messages'] = 'Книга была успешно обновлена.';
            if ($id === null) {
                $response['messages'] = 'Книга «' . $model->title . '» была успешно создана.';
            }
        } elseif ($model->hasErrors()) {
            $response['messages'] = $model->errors;
        } else {
            throw new BadRequestHttpException;
        }

        return $response;
    }

    /**
     * Страница всех глав книги с id $id.
     */
    public function actionSections($id)
    {
        $book = Book::find()->select([
            'id',
            'title'
        ])->where(['id' => $id])->with('sections')->one();

        if ($book !== null) {
            return $this->render('sections', ['book' => $book]);
        } else {
            throw new NotFoundHttpException;
        }
    }

    /**
     * Удаляет книгу (ajax).
     */
    public function actionDelete($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['status' => 0];

        if (Book::find()->where(['id' => $id])->exists()) {
            if (Book::remove($id)) {
                $response['status'] = 1;
                $response['messages'] = 'Книга была успешно удалена.';
            } else {
                $response['messages'] = 'Ошибка удаления книги.';
            }
        } else {
            throw new NotFoundHttpException;
        }

        return $response;
    }
}