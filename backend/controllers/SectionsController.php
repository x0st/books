<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 7/28/2016
 * Time: 2:33 PM
 */

namespace backend\controllers;

use avega\F;
use backend\models\ImagesForm;
use common\models\Image;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Imagine;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\Section;
use yii\web\Controller;
use yii\web\UploadedFile;

class SectionsController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Обновляет имя главы по id (ajax).
     */
    public function actionUpdateSectionName($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        // новое имя главы
        $name = \Yii::$app->request->post('name');
        $response = ['status' => (int)Section::updateName($id, $name)];

        return $response;
    }

    /**
     * Удаляет главу по id (ajax).
     */
    public function actionDeleteSection($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        Section::remove($id);

        $response = ['status' => 1];

        return $response;
    }

    /**
     * Создает главу со стандартным именем для книги $id (ajax).
     */
    public function actionCreateSection($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $createSection = Section::createDefaultSection($id);

        $response = ['status' => (bool)$createSection, 'id' => $createSection];

        return $response;
    }

    /**
     * Страница просмотра главы.
     */
    public function actionView($id)
    {
        $section = Section::find()->where(['id' => $id])->with('images')->one();

        if ($section !== null) {
            return $this->render('view', ['section' => $section, 'book' => Section::findBookBySectionId($id)]);
        } else {
            throw new NotFoundHttpException;
        }
    }

    /**
     * Функция для загрузки изображений главы в главу с id $id (ajax).
     */
    public function actionUploadImages($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['status' => 0];

        if (Section::find()->where(['id' => $id])->exists()) {

            $model = new ImagesForm;
            $model->images = UploadedFile::getInstancesByName('images');
            $model->sectionId = $id;

            if ($model->validate()) {
                $setImages = $model->setImages();

                $response['status'] = 1;
                $response['messages'] = ['Все файлы были успешно загружены и сохранены.'];
                $response['savedFiles'] = $model->getSavedFiles();

                if ($setImages === false) {
                    $response['status'] = 0;
                    $response['messages'] = ['Ошибка загрузки или сохранения изображений.'];
                } elseif ($setImages === 0) {
                    $response['status'] = 2;
                    $response['messages'] = ['Некоторые изображения не были сохранены.'];
                }

            } elseif ($model->hasErrors()) {
                $response['messages'] = $model->errors;
            } else {
                throw new BadRequestHttpException;
            }
        }

        return $response;
    }

    /**
     * Удаляет изображение главы с id $id по id или name изображения (ajax).
     */
    public function actionDeleteImage($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['status' => 0];

        if (Section::find()->where(['id' => $id])->exists()) {
            $imageId = \Yii::$app->request->post('id');
            $imageName = \Yii::$app->request->post('name');

            if (Image::remove($imageId, $imageName, $id)) {
                $response['status'] = 1;
                $response['messages'] = ['Изображение было удалено успешно.'];
            }else{
                $response['messages'] = ['Ошибка удаления изображения.'];
            }
        }else{
            throw new NotFoundHttpException;
        }

        return $response;
    }

    /**
     * Обновляет порядок изображений главы (ajax).
     */
    public function actionUpdateImages($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['status' => 0];

        if (Section::find()->where(['id' => $id])->exists()) {
            $images = \Yii::$app->request->post('form');

            if (Image::updateImages($images, $id)) {
                $response['status'] = 1;
                $response['messages'] = ['Порядок изображений был успешно сохранен.'];
            }else{
                $response['messages'] = ['Ошибка сохранения.'];
            }
        }else{
            throw new NotFoundHttpException;
        }

        return $response;
    }
}